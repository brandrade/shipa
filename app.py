from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
    return "Delivering on Kubernetes with GitLab! v2"

if __name__ == "__main__":
    app.run()